import random

# Generate a random number between 0 and 1
random_number = random.random()
print("Random number between 0 and 1:", random_number)

# Generate a random integer between a range (inclusive)
start_range = 1
end_range = 10
random_integer = random.randint(start_range, end_range)
print(f"Random integer between {start_range} and {end_range}:", random_integer)

# Generate a random floating-point number between a range
start_range = 1.0
end_range = 5.0
random_float = random.uniform(start_range, end_range)
print(f"Random float between {start_range} and {end_range}:", random_float)
